<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2017 Denis Chenu <http://www.sondages.pro>
 * @copyright 2017 GIP Arifor <http://www.arifor.fr>
 * @license AGPL v3
 * @version 0.1.1
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class smtpByUser extends \ls\pluginmanager\PluginBase
{

    protected $storage = 'DbStorage';

    static protected $name = 'smtpByUser';
    static protected $description = 'Replace default smpt by a settings by each user';

    protected $settings = array(
        'allowed'=>array(
            'type'=>'boolean',
            'label'=>'If user settings are not set use this one (or global)',
            'help'=>'If other settings are empty : use global settings',
            'default'=>0,
        ),
        'emailsmtphost'=>array(
            'type'=>'string',
            'label'=>'SMTP host',
            'default'=>"",
        ),
        'emailsmtpuser'=>array(
            'type'=>'string',
            'label'=>'SMTP user',
            'default'=>"",
        ),
        'emailsmtppassword'=>array(
            'type'=>'string',
            'label'=>'SMTP password',
            'default'=>"",
        ),
        'emailsmtpssl'=>array(
            'type'=>'select',
            'label'=>'SMTP encryption',
            'default'=>"",
            'options'=>array(
                "ssl"=>"SSL",
                "tls"=>"TLS",
            ),
            'htmlOptions'=>array(
                'empty'=>"Off"
            ),
        ),
    );

    /**
    * Did it's already set controller
    */
    private $setController=false;

    public function init() {
        $this->subscribe('beforeControllerAction');

        $this->subscribe('beforeTokenEmailExtended','beforeTokenEmail');
        $this->subscribe('beforeTokenEmail');
    }

    /**
     * Add the settings for user
     */
    public function beforeControllerAction() {
        if(!$this->setController) {
            $this->setController=true;
            if( $this->event->get('controller')=='admin' && $this->event->get('action')=='user'
                && Yii::app()->getRequest()->getParam('sa')=='personalsettings'
                ) {
                $this->_saveUserSettings();
                $this->_addUserSettings();
                return;
            }
            if( $this->event->get('controller')=='admin' && $this->event->get('action')=='tokens'
                && Yii::app()->getRequest()->getParam('sa')=='email'
                ) {
                $this->_addAlert();
                return;
            }
        }
    }

    /**
     * Use the user settings for email
     */
    public function beforeTokenEmail() {
        $oEvent=$this->event;
        $iSurveyId=$oEvent->get("survey");
        $oSurvey=Survey::model()->findByPk($iSurveyId);
        if(!$oSurvey) {
            Yii::log("Invalid Survey", 'warning','application.plugins.smtpByUser.beforeTokenEmailExtended');
            return;
        }
        $this->_setUserPhpMailer($oSurvey->owner_id);
    }

    /**
     * get translation
     * @param string
     * @return string
     */
    private function _translate($string){
        return Yii::t('',$string,array(),get_class($this));
    }
    /**
     * Add this translation just after loaded all plugins
     * @see event afterPluginLoad
     */
    public function afterPluginLoad(){
        // messageSource for this plugin:
        $messageSource=array(
            'class' => 'CGettextMessageSource',
            'cacheID' => get_class($this).'Lang',
            'cachingDuration'=>3600,
            'forceTranslation' => true,
            'useMoFile' => true,
            'basePath' => __DIR__ . DIRECTORY_SEPARATOR.'locale',
            'catalog'=>'messages',// default from Yii
        );
        Yii::app()->setComponent(get_class($this),$messageSource);
    }

    /**
     * Use the user setting for PhpMailer
     * @param integer $userId
     * @return void
     */
    private function _setUserPhpMailer($userId) {
        $emailsmtphost = $this->get('emailsmtphost','User',$userId);
        $emailsmtpuser = $this->get('emailsmtpuser','User',$userId);

        if(empty($emailsmtpuser) && !$this->get('allowed')) {
            $message=sprintf($this->_translate("No user set for user %s in Personnal settings"),$userId);
            if($userId == Yii::app()->session['loginID']) {
                $url = Yii::app()->createAbsoluteUrl("admin/user",array('sa'=>'personalsettings'));
                $message=sprintf($this->_translate("No user set in your Personnal settings : %s"),$url);
            }
            $this->event->set('error',$message);
            $this->event->set('send',false);
            return;
        }
        if(!empty($emailsmtpuser) || !empty($emailsmtphost)) {
            App()->setConfig("emailmethod",'smtp');
            App()->setConfig("emailsmtphost",$emailsmtphost);
            App()->setConfig("emailsmtpuser",$emailsmtpuser);
            App()->setConfig("emailsmtppassword",$this->get('emailsmtppassword','User',$userId));
            App()->setConfig("emailsmtpssl",$this->get('emailsmtpssl','User',$userId));
            return;
        }
        $emailsmtphost = $this->get('emailsmtphost',null,null);
        $emailsmtpuser = $this->get('emailsmtpuser',null,null);
        if(!empty($emailsmtpuser) || !empty($emailsmtphost)) {
            App()->setConfig("emailmethod",'smtp');
            App()->setConfig("emailsmtphost",$emailsmtphost);
            App()->setConfig("emailsmtpuser",$emailsmtpuser);
            App()->setConfig("emailsmtppassword",$this->get('emailsmtppassword',null,null));
            App()->setConfig("emailsmtpssl",$this->get('emailsmtpssl',null,null));
            return;
        }
    }
    
    /**
     * Add user setting for this plugin
     * @return void
     */
    private function _addUserSettings() {
        Yii::setPathOfAlias('smtpByUser', dirname(__FILE__));
        $userId=Yii::app()->session['loginID'];
        $aData=array(
            'emailsmtphost' => $this->get('emailsmtphost','User',$userId),
            'emailsmtpuser' => $this->get('emailsmtpuser','User',$userId),
            'emailsmtppassword' => $this->get('emailsmtppassword','User',$userId),
            'emailsmtpssl' => $this->get('emailsmtpssl','User',$userId),
            'emailsmtppassword_placeholder'=> "Leave empty to didn't update",
        );
        $view=Yii::app()->controller->renderPartial('smtpByUser.views.personalMailSettings',$aData,1);
        $script="$('#personalsettings').append(".json_encode($view).");";
        Yii::app()->getClientScript()->registerScript("smtpByUserSettings",$script,CClientScript::POS_READY);
        
    }
    /**
     * Save user setting for this plugin
     * @return void
     */
    private function _saveUserSettings() {
        if(!App()->getRequest()->getIsPostRequest()){
            return;
        }
        $smtpByUserSettings=Yii::app()->getRequest()->getPost('smtpByUser');
        $userId=Yii::app()->session['loginID'];
        if(!empty($smtpByUserSettings)) {
            foreach($smtpByUserSettings as $name=>$value) {
                switch ($name) {
                    case 'emailsmtppassword':
                        if($value!="") {
                            $this->set($name, $value, 'User', $userId);
                        }
                        break;
                    default:
                        $this->set($name, $value, 'User', $userId);
                }
             }
        }
    }

    /**
     * Add an alert about Personnal setting
     */
    private function _addAlert() {
        $iSurveyId=(int) App()->getRequest()->getParam('surveyid');
        $oSurvey=Survey::model()->findByPk($iSurveyId);
        if(!$oSurvey) {
            return;
        }
        if($this->get('allowed')) {
            return;
        }
        $userId=$oSurvey->owner_id;
        $emailsmtpuser = $this->get('emailsmtpuser','User',$userId);
        if(!$emailsmtpuser) {
            $message=sprintf($this->_translate("No user set for user %s in Personnal settings"),$userId);
            if($userId == Yii::app()->session['loginID']) {
                $url = Yii::app()->createUrl("admin/user",array('sa'=>'personalsettings'));
                $message=sprintf($this->_translate("No user set in your <a href='%s'>Personnal settings</a>"),$url);
            }
            Yii::app()->setFlashMessage($message,'error');
        }
    }

}
