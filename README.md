# smtpByUser : SMTP settings by user. #

Each user can set own SMTP settings. You can choose if user must set a smptp to send email or be allowed to use default settings.

## Usage

- Global settings
    - In global settings, you just have to choose if user must have own smtp settings or not.
    - You can set up a smptp account used only for token email
- User settings
    - Each user have new settings for a smtp account. If no user was set : user can be allowed to use default settings.
- Survey settings
    - When send email for token : smtp used was the one of the owner of the survey.

## Installation

### Via GIT
- Go to your LimeSurvey Directory
- Clone in plugins/smtpByUser directory : `git clone https://git.framasoft.org/SondagePro-LimeSurvey-plugin/smtpByUser.git smtpByUser`

### Via ZIP dowload
- Download <http://extensions.sondages.pro/IMG/auto/smtpByUser.zip>
- Extract : `unzip smtpByUser.zip`
- Move the directory to  plugins/ directory inside LimeSurvey

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2017 Denis Chenu <http://sondages.pro>
- Copyright © 2017 GIP Arifor <http://www.arifor.fr>
- Licence : GNU Affero General Public License <https://www.gnu.org/licenses/agpl-3.0.html>
